const express = require("express");
const session = require("express-session");
const cookieParser = require("cookie-parser");
const path = require('path')
const Sequelize = require("sequelize");
const SequelizeStore = require("connect-session-sequelize")(session.Store);
const dotenv = require('dotenv');
dotenv.config();
const config = require('./server/config/config')
const sequelize = new Sequelize(config.database, config.username, config.password, {
    dialect: config.dialect
});

const router = require("./routes");

const app = express();
const port = process.env.APP_PORT
const public = process.env.PUBLIC

const sessionStore = new SequelizeStore({
    db: sequelize
});



app.use(session({
    secret: 'secret',
    store: sessionStore,
    saveUninitialized: true,
    resave: false,
}));

sessionStore.sync()

app.use(express.static(public))


app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

app.use(cookieParser())
app.use(router);

app.listen(port, () => {
    console.log(`App listening on port: ${port}`);
})
const roomController = require('../controllers/roomController')
const webMiddleware = require('../middlewares/web.middleware')

const router = require('express').Router()

router.get('/', webMiddleware.redirectIfSigned, roomController.index)

module.exports = router
const router = require('express').Router()
const homeRouter = require('./homeRouter')
const roomRouter = require('./roomRouter')
const profileRouter = require('./profileRouter')

const apiUserRouter = require('./api/userRouter')
const apiProfileRouter = require('./api/profileRouter')

const apiV1UserRouter = require('./api/v1/userRouter')
const apiV1ProfileRouter = require('./api/v1/profileRouter')
const apiV1RoomRouter = require('./api/v1/roomRouter')
const apiV1FightRouter = require('./api/v1/fightRouter')
// MVC
router.use('/', homeRouter)
router.use('/room', roomRouter)
router.use('/profile', profileRouter)

// MCR
router.use('/api/user', apiUserRouter)
router.use('/api/profile', apiProfileRouter)

// API
router.use('/api/v1/user', apiV1UserRouter)
router.use('/api/v1/profile', apiV1ProfileRouter)
router.use('/api/v1/room', apiV1RoomRouter)
router.use('/api/v1/fight', apiV1FightRouter)

module.exports = router
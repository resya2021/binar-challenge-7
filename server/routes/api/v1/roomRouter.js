const roomControllerV1 = require('../../../controllers/api/v1/roomController')
const { verifyJWT } = require('../../../middlewares/api.middleware')
const bodyParser = require('body-parser')

const jsonParser = bodyParser.json()
const router = require('express').Router()

router.get('', jsonParser, verifyJWT, roomControllerV1.index)
router.post('/create', jsonParser, verifyJWT, roomControllerV1.create)

module.exports = router
const profileControllerV1 = require('../../../controllers/api/v1/profileController')
const { verifyJWT } = require('../../../middlewares/api.middleware')
const bodyParser = require('body-parser')

const jsonParser = bodyParser.json()
const router = require('express').Router()

router.get('', jsonParser, verifyJWT, profileControllerV1.index)
router.get('/gamehistory', jsonParser, verifyJWT, profileControllerV1.gamehistory)
router.post('/create', jsonParser, verifyJWT, profileControllerV1.create)
router.post('/edit/:id', jsonParser, verifyJWT, profileControllerV1.edit)
router.post('/delete/:id', jsonParser, verifyJWT, profileControllerV1.delete)

module.exports = router
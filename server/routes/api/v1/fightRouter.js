const fightControllerV1 = require('../../../controllers/api/v1/fightController')
const { verifyJWT } = require('../../../middlewares/api.middleware')
const bodyParser = require('body-parser')

const jsonParser = bodyParser.json()
const router = require('express').Router()

router.post('/join', jsonParser, verifyJWT, fightControllerV1.join)
router.post('/:code', jsonParser, verifyJWT, fightControllerV1.fight)

module.exports = router
const bodyParser = require('body-parser')
const userController = require('../../controllers/api/userController')
const { verifyJWT } = require('../../middlewares/api.middleware')

const urlEncoded = bodyParser.urlencoded({extended:false})
const router = require('express').Router()

router.post('/login', urlEncoded, userController.login)
router.post('/register', urlEncoded, userController.register)
router.get('/logout', urlEncoded, userController.logout)

module.exports = router
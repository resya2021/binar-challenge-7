const router = require('express').Router()
const bodyParser = require('body-parser')
const profileController = require('../../controllers/api/profileController')

const jsonParser = bodyParser.json()
const urlEncoded = bodyParser.urlencoded({extended:false})

router.post('/create', urlEncoded, profileController.create)
router.post('/edit', urlEncoded, profileController.edit)
router.get('/delete/:id', profileController.delete)

module.exports = router
const profileController = require('../controllers/profileController')
const webMiddleware = require('../middlewares/web.middleware')
const router = require('express').Router()

router.get('/', webMiddleware.verifyWebSession, profileController.index)
router.get('/create', webMiddleware.verifyWebSession, profileController.create)
router.get('/edit/:id', webMiddleware.verifyWebSession, profileController.edit)
router.get('/gamehistory/:id', webMiddleware.verifyWebSession, profileController.show)

module.exports = router
const router = require('express').Router()
const homeController = require('../controllers/homeController')
const webMiddleware = require('../middlewares/web.middleware')

router.get('/', webMiddleware.isLoggedIn, homeController.index)
router.get('/login', webMiddleware.isLoggedIn, homeController.login)
router.get('/register', webMiddleware.isLoggedIn, homeController.register)

module.exports = router
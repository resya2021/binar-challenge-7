const { User } = require('../models')

module.exports = {
    index: async (req, res) => {
        res.render('index', {
            session: req.session
        })
    },
    login: async (req, res) => {
        res.render('login', {
            session: req.session
        })
    },
    register: async (req, res) => {
        res.render('register', {
            session: req.session
        })
    }
}
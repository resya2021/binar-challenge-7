const { User, userBiodata, userGameHistory } = require('../../models')

module.exports = {
    create: async (req, res) => {
        try {
            let profile = await userBiodata.create({
                userId: req.session.userId,
                fullName: req.body.fullName,
                gender: req.body.gender,
                dateofbirth: req.body.dateofbirth
            })
            res.redirect('/profile')
        } catch (error) {
            res.render('profiles/create', {
                error: error
            })
        }
    },
    edit: async (req, res) => {
        try {
            let profile = await userBiodata.findByPk(req.body.id)
            profile.fullName = req.body.fullName ? req.body.fullName : profile.fullName
            profile.dateofbirth = req.body.dateofbirth ? req.body.dateofbirth : profile.dateofbirth
            
            await profile.save()

            res.redirect('/profile')
            

        } catch (error) {
            res.render('profiles/edit', {
                error: error
            })
        }
    },
    delete: async (req, res) => {
        try {
            const count = await userBiodata.destroy({
                where: { id: req.params.id }
            })

            if(count > 0) {
                return res.redirect('/profile')
            } else {
                return res.redirect('/profile')
            }
        } catch (error) {
            return res.redirect('/')
        }
    }
}
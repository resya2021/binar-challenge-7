const { User } = require('../../models')

module.exports = {
    login: async(req, res) => {
        let message = {
            type: 'error',
            message: 'Login failed, incorrect username / password'
        }
        try {
            let user = await User.findOne({
                where: { username: req.body.username }
            })
    
            if(!user.validPassword(req.body.password)) {
                return res.render('login', {
                    message:message
                })
            }

            req.session.userId = user.id 
            req.session.username = user.username 
            req.session.role = user.role 

            res.redirect('/room')
        } catch (error) {
            return res.render('login', {
                message: message
            })
        }
    },
    register: async (req, res) => {
        try {
            let user = await User.create({
                username: req.body.username,
                password: req.body.password,
                role: 'User',
                isJoin: false
            });
            
            res.redirect("/login");
        } catch (error) {
            if (error.name === 'SequelizeUniqueConstraintError') {
                return res.status(400).json({ message: 'Username already in use' });
              }
            res.render("register", {
                message: {
                    type: 'error',
                    message: error
                }
            })
        }
    },
    logout: async (req, res) => {
        if (req.session.userId) {
            req.session.destroy()
            res.redirect('/')
        }
    }

}
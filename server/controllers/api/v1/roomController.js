const { User, room } = require('../../../models')
const crypto = require('crypto')

module.exports = {
    index: async (req, res) => {
        let orderQuery = [
            ['updatedAt', 'desc'],
            ['createdAt', 'desc']
        ]

        await room.findAll({
            where: { isActive: true},
            order: orderQuery
        })
        .then(rooms => {
            return res.status(200).send({
                rooms: rooms
            })
        })
    },
    create: async(req, res) => {
        try {
            function generateCode(input) {
                if(!input) {
                    return null
                }
                const hash = crypto.createHash('sha1')
                hash.update(input)
                const hex = hash.digest('hex')
                const code = parseInt(hex.slice(0, 4), 16)
                return code.toString().padStart(4, '0')
            }
            let Room = await room.create({
                code: generateCode(req.body.roomname),
                creatorId: req.user.id,
                opponent_id: req.body.opponent_id,
                isActive: true,
                createdAt: new Date(),
                updatedAt: new Date()
            })
            return res.status(201).send({
                message: 'Room successfully created',
                code: Room.code,
                opponentID : Room.opponent_id
            })
        } catch (error) {
            return res.status(400).send({
                message: error
            })
        }
    }
}
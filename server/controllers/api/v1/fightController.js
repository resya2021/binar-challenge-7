const { User, userGameHistory, room, fight } = require('../../../models')
const { Op } = require('sequelize')

module.exports = {
    join: async(req, res) => {
        try {
            const roomcode = req.body.code
            const userId = req.user.id

            let whereQuery = {
                code: roomcode,
                [Op.or]: [{ creatorId: userId}, {opponent_id: userId}]
            }
            let Room = await room.findOne({
                where: whereQuery
            })
            if(Room) {
                let Fight = await fight.findOne({
                    where: { roomId: Room.id }
                })
                if(Fight) {
                    res.status(200).send({
                        message: 'The game already started',
                        message2: `Try /api/v1/fight/${roomcode} to input your first move`
                    })
                } else {
                    let Fight = await fight.create({
                        roomId: Room.id,
                        round: 0,
                        player1_choice: null,
                        player2_choice: null,
                        winner: null,
                        createdAt: new Date(),
                        updatedAt: new Date()
                    })
                    res.status(200).send({
                        message: 'The game has begun..',
                        message2: `Try /api/v1/fight/${roomcode} to input your first move`
                    })
                }
            } else {
                res.status(401).json({ message : 'User is unathorized to join the room'})
            }
        } catch (error) {
            res.status(500).send({
                message: error.message
            })
        }
    },
    fight: async(req, res) => {
        try {
            const roomcode = req.params.code
            const userId = req.user.id
            const move = req.body.move
    
            let whereQuery = {
                code: roomcode,
                [Op.or]: [{ creatorId: userId}, {opponent_id: userId}]
            }
            let Room = await room.findOne({
                where: whereQuery
            })
            
            if(Room) {
                let Fight = await fight.findOne({
                    where: {roomId: Room.id}
                })
                
                if(Fight && Fight.round < 3) {
                    
                    if(Room.creatorId === userId && !Fight.player1_choice) {
                        Fight.player1_choice = move
                        await Fight.save()
                    } else if(Room.opponent_id === userId && !Fight.player2_choice) {
                        Fight.player2_choice = move
                        await Fight.save()
                    } else {
                        return res.status(400).send({
                            message: 'Move successfully made, please wait for opponent to make their move',
                            round: Fight.round
                        })
                    }
    
                    if(Fight.player1_choice && Fight.player2_choice) {
                        if(Fight.player1_choice === 'rock' && Fight.player2_choice === 'scissor' ||
                            Fight.player1_choice === 'paper' && Fight.player2_choice === 'rock' ||
                            Fight.player1_choice === 'scissor' && Fight.player2_choice === 'paper') {
                            winner = 'player1'
                        } else if(Fight.player1_choice === Fight.player2_choice) {
                            winner = 'tie'
                        } else {
                            winner = 'player2'
                        }
    
                        let user1History = await userGameHistory.create({
                            userId: Room.creatorId,
                            opponentId: Room.opponent_id,
                            player_choice: Fight.player1_choice,
                            opponent_choice: Fight.player2_choice,
                            result: winner,
                            createdAt: new Date(),
                            updatedAt: new Date()
                        })
                        let user2History = await userGameHistory.create({
                            userId: Room.opponent_id,
                            opponentId: Room.creatorId,
                            player_choice: Fight.player2_choice,
                            opponent_choice: Fight.player1_choice,
                            result: winner === 'player1' ? 'player2' : winner === 'player2' ? 'player1' : winner,
                            createdAt: new Date(),
                            updatedAt: new Date()
                        })
    
                        
                        Fight.player1_choice = null
                        Fight.player2_choice = null
                        Fight.round += 1
                        Fight.winner = null
                        await Fight.save()
    
                        return res.status(200).send({
                            message: 'And we deciding the winner for this round is..',
                            winner: winner,
                            Player1: user1History.player_choice, 
                            Player2: user2History.player_choice
                        })
                    } else {
                        return res.status(200).send({
                            message: 'Move successfully made, waiting for opponent to make their move'
                        })
                    }
                } else if(Fight && Fight.round >= 3) {
                    Room.isActive = false
                    await Room.save()
                    res.status(400).send({
                        message: 'The room is no longer available, please make another room',
                        message2: 'If you want to check the result, go to api/v1/profile/gamehistory'
                    })
                } else {
                    res.status(400).send({
                        message: `One of the players need to made POST request to /api/v1/fight/join with "code":${Room.code} as input`
                    })
                }
            } else {
                res.status(401).send({
                    message: 'Unauthorized to join the room'
                })
            }
        } catch (error) {
            res.status(500).send({
                message: error.message
            })
        }
    }
}
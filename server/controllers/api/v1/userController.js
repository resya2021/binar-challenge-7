const { User } = require('../../../models')

module.exports = {
    login: async (req, res) => {
        let message = 'Login failed, incorrect username or password'

        try {
            let user = await User.findOne({
                where: { username: req.body.username }
            })

            user.validPasswordPromise(req.body.password)
                .then(resolve => {
                    return res.json({
                        message: 'Login successfully',
                        token: user.generateToken()
                    })
                })
                .catch(reject => {
                    return res.status(400).json({
                        message: 'Failed to login'
                    })
                })

        } catch (error) {
            console.log(error)
            return res.status(500).json({
                message: 'Internal server error'
            })
        }
    },
    register: async (req, res) => {
        try {
            let user = await User.create({
                username: req.body.username,
                password: req.body.password,
                role: 'User'
            })

            return res.json({
                message: 'User created successfully'
            })
        } catch (err) {
            console.log(err)
            return res.status(400).json({
                message: 'Failed to create user',
                error: err.message
            })
        }
    },
    verify: async (req, res) => {
        return res.status(200).json({
            message: 'Success'
        })
    }
}
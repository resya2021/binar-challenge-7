const { User, userBiodata, userGameHistory } = require('../../../models')

module.exports = {
    index: async (req, res) => {
        let whereQuery = req.user.role === 'Super Admin' ? {} : { userId : req.user.id }

        let orderQuery = [
            ['updatedAt', 'desc'],
            ['createdAt', 'desc']
        ]

        userBiodata.findAll({
            where: whereQuery,
            order: orderQuery
        })
        .then(profiles => {
            return res.status(200).send({
                profiles: profiles
            })
        })
    },
    gamehistory: async (req, res) => {
        let whereQuery = {userId:req.user.id}

        let orderQuery = [
            ['updatedAt', 'desc'],
            ['createdAt', 'desc']
        ]

        userGameHistory.findAll({
            where: whereQuery,
            order: orderQuery,
        })
        .then(history => {
            return res.status(200).send({
                history: history
            })
        })
    },
    create: async (req, res) => {
        try {
            let profileCreated = await userBiodata.findOne({
                where: {userId: req.user.id}
            })
            if(!profileCreated) {
                let profile = await userBiodata.create({
                    userId: req.user.id,
                    fullName: req.body.fullName,
                    gender: req.body.gender,
                    dateofbirth: req.body.dateofbirth,
                    createdAt: new Date(),
                    updatedAt: new Date()
                })
    
                return res.status(201).send({
                    message: 'Biodata created',
                    profile: profile
                })
            } else {
                res.status(400).send({
                    message: 'Error: You can only have one bioadata at the time'
                })
            }
        } catch (error) {
            return res.status(400).send({
                message: error.message
            })
        }
    },
    edit: async (req, res) => {
        try {
            let profile = await userBiodata.findByPk(req.params.id)
            if(req.user.role === 'Super Admin' || profile.userId === req.user.id) {
                profile.fullName = req.body.fullName ? req.body.fullName : profile.fullName
                profile.gender = req.body.gender ? req.body.gender : profile.gender
                profile.dateofbirth = req.body.dateofbirth ? req.body.dateofbirth : profile.dateofbirth
                await profile.save()
    
                return res.status(200).send({
                    message: 'Biodata updated',
                    profile: profile
                })
            } else {
                res.status(401).send({
                    message: 'Unauthorized'
                })
            }
        } catch (error) {
            return res.status(400).send({
                message: error.message
            })
        }
    },
    delete: async (req, res) => {
        try {
            const biodata = await userBiodata.findOne({
                where: { id: req.params.id }
              });
              
            if(req.user.role === 'Super Admin' || biodata.userId === req.user.id) {
                const count = await userBiodata.destroy({
                    where: { id: req.params.id }
                })
                if (count > 0) {
                    return res.status(200).send({
                        message: 'Biodata Deleted'
                    })
                } else {
                    return res.status(400).send({
                        message: 'Error deleting biodata'
                    })
                }
            } else return res.status(401).send({
                message: "Unauthorized"
            })
        } catch (error) {
            return res.status(400).send({
                message: error.message
            })
        }
    }
}
const { where } = require('sequelize');
const { User, userBiodata, userGameHistory } = require('../models')

module.exports = {
    index: async (req, res) => {
        let whereQuery = req.session.role === 'Super Admin' ? {} : {userId : req.session.userId};

        let orderQuery = [
            ['updatedAt', 'desc'],
            ['createdAt', 'desc']
        ]

        userBiodata.findAll({
            where: whereQuery,
            order: orderQuery
        })

        .then(profiles => {
            res.render('profiles/index', {
                session: req.session,
                profiles: profiles
            })
        })
    },
    create: async (req, res) => {
        res.render('profiles/create', {
            session: req.session
        })
    },
    edit: async (req, res) => {
        userBiodata.findByPk(req.params.id)
        .then(profile => {
            res.render('profiles/edit', {
                session: req.session,
                profile: profile
            })
        })
    },
    show: async (req, res) => {
        let whereQuery = req.session.role === 'Super Admin' ? {} : { userId: req.session.userId }
        let orderQuery = [
            ['updatedAt', 'desc'],
            ['createdAt', 'desc']
        ]

        userGameHistory.findAll({
            where: whereQuery,
            order: orderQuery
        })
        .then(histories => {
            res.render('profiles/gamehistory', {
                session: req.session,
                histories: histories
            })
        })
    }
}
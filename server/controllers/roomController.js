const { room } = require('../models')

module.exports = {
    index: async (req, res) => {

        let orderQuery = [
            ['updatedAt', 'desc'],
            ['createdAt', 'desc']
        ];

        room.findAll({
            where: {isActive: true},
            order: orderQuery,
        })
        .then(rooms => {
            res.render('room/index', {
                session: req.session,
                rooms: rooms
            })
        })
    }
}
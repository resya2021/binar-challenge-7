'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert('userGameHistories', [{
        userId: 1,
        opponentId: 2,
        player_choice: "rock",
        opponent_choice: 'paper',
        result: 'You Lose',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        userId: 2,
        opponentId: 1,
        player_choice: "scissor",
        opponent_choice: 'paper',
        result: 'You Win',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        userId: 1,
        opponentId: 3,
        player_choice: "paper",
        opponent_choice: 'paper',
        result: 'Tie',
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ])
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
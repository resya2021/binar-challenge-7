'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert('rooms', [{
        code: 'ABCD',
        creatorId: 1,
        opponent_id: 2,
        isActive: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        code: 'BCDF',
        creatorId: 2,
        opponent_id: 3,
        isActive: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        code: 'XYZA',
        creatorId: 2,
        opponent_id: 1,
        isActive: true,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ])
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
'use strict';
const bcrypt = require('bcrypt')

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    const password = await bcrypt.hashSync('123456', 10)
    await queryInterface.bulkInsert('Users',
      [{
          username: 'Admin321',
          password: password,
          role: 'Super Admin',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          username: 'User111',
          password: password,
          role: 'User',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          username: 'master69',
          password: password,
          role: 'User',
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ]
    )
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert('userBiodata', [{
        userId: 1,
        fullName: "Super Admin",
        gender: 'm',
        dateofbirth: new Date(9, 2, 1970),
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        userId: 2,
        fullName: "Regular User",
        gender: 'm',
        dateofbirth: new Date(3, 2, 2005),
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        userId: 3,
        fullName: "eleanor",
        gender: 'f',
        dateofbirth: new Date(12, 2, 2002),
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ])
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
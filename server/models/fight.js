'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class fight extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      fight.belongsTo(models.room, {
        foreignKey: 'roomId',
        onDelete: 'CASCADE'
      })
    }
  }
  fight.init({
    roomId: DataTypes.INTEGER,
    round: DataTypes.INTEGER,
    player1_choice: DataTypes.STRING,
    player2_choice: DataTypes.STRING,
    winner: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'fight',
  });

  return fight;
};
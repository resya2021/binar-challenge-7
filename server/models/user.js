'use strict';
const {
  Model
} = require('sequelize');

const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.hasMany(models.userGameHistory, {
        foreignKey: 'userId',
        onDelete: 'CASCADE'
      })
      User.hasOne(models.userBiodata, {
        foreignKey: 'userId',
        onDelete: 'CASCADE'
      })
    }
  }
  User.init({
    username: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: {
        args: true,
        msg: 'Username already in use'
      },
      validate: {
        notEmpty: {
          msg: "Username is required"
        },
        isAlphanumeric: true,
        len: {
          args: [5, 20],
          msg: "Username must be between 5 and 20 characters"
        },
        isUnique: (value, next) => {
          User.findAll({
              where: {
                username: value
              }
            })
            .then((user) => {
              if (user.length != 0) {
                next(new Error('username already in use'));
              }
              next();
            });
        }
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: "Password is required"
        },
        len: {
          args: [5, 20],
          msg: "Password must be between 5 and 20 characters"
        }
      }
    },
    role: DataTypes.STRING,
    isJoin: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    }
  }, {
    sequelize,
    modelName: 'User',
  });

  User.addHook('afterValidate', (user, options) => {
    const salt = bcrypt.genSaltSync()
    user.password = bcrypt.hashSync(user.password, salt)
  })

  User.prototype.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password)
  }

  User.prototype.validPasswordPromise = function (password) {
    return new Promise((resolve, reject) => {
      if (bcrypt.compareSync(password, this.password)) {
        resolve('password is valid');
      } else {
        reject('password invalid');
      }
    });
  }

  User.prototype.generateToken = function () {
    const payload = {
      id: this.id,
      username: this.username,
      role: this.role
    };

    const token = jwt.sign(payload, process.env.JWT_SECRET || 'secret', {
      expiresIn: '1 day'
    });
    return token;
  }
  return User;
};
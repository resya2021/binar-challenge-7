'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class userGameHistory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      userGameHistory.belongsTo(models.User, {
        foreignKey: 'userId',
        onDelete: 'CASCADE'
      })
      userGameHistory.belongsTo(models.User, {
        foreignKey: 'opponentId',
        onDelete: 'CASCADE'
      })
    }
  }
  userGameHistory.init({
    userId: DataTypes.INTEGER,
    opponentId: DataTypes.INTEGER,
    player_choice: DataTypes.STRING,
    opponent_choice: DataTypes.STRING,
    result: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'userGameHistory',
  });
  return userGameHistory;
};
'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      room.belongsTo(models.User, {
        foreignKey: 'creatorId',
        onDelete: 'CASCADE'
      })
      room.belongsTo(models.User, {
        foreignKey: 'opponent_id',
        onDelete: 'CASCADE'
      })
      room.hasMany(models.fight, {
        foreignKey: 'roomId',
        onDelete: 'CASCADE'
      })
    }
  }
  room.init({
    code: DataTypes.STRING,
    creatorId: DataTypes.INTEGER,
    opponent_id: DataTypes.INTEGER,
    isActive: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'room',
  });
  return room;
};
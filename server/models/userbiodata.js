'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class userBiodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      userBiodata.belongsTo(models.User, {
        foreignKey: 'userId',
        onDelete: 'CASCADE'
      })
    }
  }
  userBiodata.init({
    userId: DataTypes.INTEGER,
    fullName: DataTypes.STRING,
    gender: {
      type: DataTypes.ENUM,
      values: ['m', 'f', 'null'],
      validate: {
        isIn: [
          ['m', 'f', 'null']
        ]
      }
    },
    dateofbirth: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'userBiodata',
  });
  return userBiodata;
};